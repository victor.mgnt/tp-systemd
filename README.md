# TP systemd

## I. `systemd`-basics

### 1. First steps

#### vérifier que la version de `systemd` est > 241

``` bash
admin in ~ at fedora-tp-leo
➜ systemctl --version
systemd 243 (v243.4-1.fc31)
+PAM +AUDIT +SELINUX +IMA -APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=unified
```

#### 🌞 s'assurer que systemd est PID1

``` bash
admin in ~ at fedora-tp-leo
➜ _ systemctl status init.scope
● init.scope - System and Service Manager
   Loaded: loaded
Transient: yes
   Active: active (running) since Thu 2019-12-05 10:48:27 CET; 19min ago
     Docs: man:systemd(1)
    Tasks: 1 (limit: 4688)
   Memory: 48.5M
   CGroup: /init.scope
           └─1 /usr/lib/systemd/systemd --switched-root --system --deserialize 28
```

#### 🌞 check tous les autres processus système (PAS les processus kernel)

``` bash
admin in ~ at fedora-tp-leo
➜ sudo systemctl status
● fedora-tp-leo
    State: degraded
     Jobs: 0 queued
   Failed: 1 units
    Since: Thu 2019-12-05 10:48:29 CET; 23min ago
   CGroup: /
           ├─user.slice
           │ └─user-1000.slice
           │   ├─user@1000.service
           │   │ └─init.scope
           │   │   ├─911 /usr/lib/systemd/systemd --user
           │   │   └─913 (sd-pam)
           │   ├─session-3.scope
           │   │ ├─ 2808 sshd: admin [priv]
           │   │ ├─ 2811 sshd: admin@pts/0
           │   │ ├─ 2812 -zsh
           │   │ ├─10077 sudo systemctl status
           │   │ ├─10080 systemctl status
           │   │ └─10081 less
           │   └─session-1.scope
           │     ├─1294 gpg-agent --daemon --use-standard-socket
           │     └─1380 ssh-agent -s
           ├─init.scope
           │ └─1 /usr/lib/systemd/systemd --switched-root --system --deserialize 28
           └─system.slice
             ├─containerd.service
             │ └─647 /usr/bin/containerd
             ├─systemd-udevd.service
             │ └─518 /usr/lib/systemd/systemd-udevd
             ├─dbus-broker.service
             │ ├─620 /usr/bin/dbus-broker-launch --scope system --audit
             │ └─621 dbus-broker --log 4 --controller 9 --machine-id be1eb5b0480b41e294953b05585e2abd --max-bytes 536870912 --max-fds 4096 --max-matches 131072 --audit
             ├─docker.service
             │ └─663 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
             ├─chronyd.service
             │ └─616 /usr/sbin/chronyd
             ├─auditd.service
             │ └─593 /sbin/auditd
             ├─systemd-journald.service
             │ └─503 /usr/lib/systemd/systemd-journald
             ├─sshd.service
             │ └─674 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openssh.com,aes128-ctr,aes128-cbc>
             ├─NetworkManager.service
             │ └─625 /usr/sbin/NetworkManager --no-daemon
             ├─firewalld.service
             │ └─613 /usr/bin/python3 /usr/sbin/firewalld --nofork --nopid
             ├─sssd.service
             │ ├─722 /usr/sbin/sssd -i --logger=files
             │ ├─732 /usr/libexec/sssd/sssd_be --domain implicit_files --uid 0 --gid 0 --logger=files
             │ └─737 /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=files
             ├─system-getty.slice
             │ └─getty@tty1.service
             │   └─693 /sbin/agetty -o -p -- \u --noclear tty1 linux
             └─systemd-logind.service
               └─741 /usr/lib/systemd/systemd-logind
```

##### décrire brièvement au moins 5 autres processus système

- chronyd : gestion de la synchronisation du temps NTP
- firewalld : pare-feu
- sshd : connexion à distance au serveur par SSH
- systemd-logind : gestion de la connexion des utilisateurs
- systemd-journald : collecte et stockage des évènements kernel, système et applicatif

### 2. Gestion du temps

#### `timedatectl` sans argument fournit des informations sur la machine

``` bash
admin in ~ at fedora-tp-leo
➜ timedatectl
               Local time: Thu 2019-12-05 11:36:38 CET
           Universal time: Thu 2019-12-05 10:36:38 UTC
                 RTC time: Thu 2019-12-05 10:36:38
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

#### 🌞 déterminer la différence entre Local Time, Universal Time et RTC time

Le temps RTC est le temps calculé par l'horloge interne de la carte mère.
Le temps universel est le temps standardisé comme le temps solaire moyen au méridien de Greenwich (très vite résumé).
Le temps local est le temps de la zone temporelle configurée par rapport au temps universel.

##### expliquer dans quels cas il peut être pertinent d'utiliser le RTC time

Il est pertinent d'utiliser l'horloge interne pour les systèmes déconnectés du réseau, comme les systèmes embarqués.

#### 🌞 changer de timezone pour un autre fuseau horaire européen

``` bash
admin in ~ at fedora-tp-leo
➜ sudo timedatectl set-timezone UTC

admin in ~ at fedora-tp-leo
➜ timedatectl
               Local time: Thu 2019-12-05 10:59:48 UTC
           Universal time: Thu 2019-12-05 10:59:48 UTC
                 RTC time: Thu 2019-12-05 10:59:48
                Time zone: UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

#### 🌞 désactiver le service lié à la synchronisation du temps avec `timedatectl set-ntp <BOOLEAN>`, et vérifier à la main qu'il a été coupé

``` bash
admin in ~ at fedora-tp-leo
➜ sudo timedatectl set-ntp false

admin in ~ at fedora-tp-leo
➜ timedatectl
               Local time: Thu 2019-12-05 11:00:30 UTC
           Universal time: Thu 2019-12-05 11:00:30 UTC
                 RTC time: Thu 2019-12-05 11:00:30
                Time zone: UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: inactive
          RTC in local TZ: no
```

Le service NTP est *inactive*.

### 3. Gestion de noms

#### changer les noms de la machine avec `hostnamectl set-hostname`

``` bash
admin in ~ at fedora-tp-leo
➜ hostnamectl
   Static hostname: fedora-tp-leo
         Icon name: computer-vm
           Chassis: vm
        Machine ID: be1eb5b0480b41e294953b05585e2abd
           Boot ID: 58b4b45d76164318a82bc137f56d45ce
    Virtualization: kvm
  Operating System: Fedora 31 (Thirty One)
       CPE OS Name: cpe:/o:fedoraproject:fedora:31
            Kernel: Linux 5.3.12-300.fc31.x86_64
      Architecture: x86-64

admin in ~ at fedora-tp-leo
➜ sudo hostnamectl set-hostname fedora-tp

admin in ~ at fedora-tp-leo
➜ hostnamectl
   Static hostname: fedora-tp
         Icon name: computer-vm
           Chassis: vm
        Machine ID: be1eb5b0480b41e294953b05585e2abd
           Boot ID: 58b4b45d76164318a82bc137f56d45ce
    Virtualization: kvm
  Operating System: Fedora 31 (Thirty One)
       CPE OS Name: cpe:/o:fedoraproject:fedora:31
            Kernel: Linux 5.3.12-300.fc31.x86_64
      Architecture: x86-64
```

#### 🌞 expliquer la différence entre `--pretty`, `--static` et `--transient`. Lequel est à utiliser pour des machines de prod ?

Le nom d'hôte *static* est le nom d'hôte traditionnel, qui peut être choisi par l'utilisateur, et est stocké dans le fichier /etc/hostname. Le nom d'hôte *transient* est un nom d'hôte dynamique maintenu par le noyau. Il est initialisé au nom d'hôte *static* par défaut, dont la valeur par défaut est *localhost*. Il peut être modifié par DHCP au moment de l'exécution. Le nom d'hôte *pretty* est un nom d'hôte utf-8 de forme libre pour présentation à l'utilisateur.

Il vaut mieux utiliser le nom d'hôte *static* en production.

#### on peut aussi modifier certains autre noms comme le type de déploiement : `hostnamectl set-deployment <NAME>`

``` bash
admin in ~ at fedora-tp-leo
➜ sudo hostnamectl set-deployment development

admin in ~ at fedora-tp-leo
➜ hostnamectl
   Static hostname: fedora-tp
         Icon name: computer-vm
           Chassis: vm
        Deployment: development
        Machine ID: be1eb5b0480b41e294953b05585e2abd
           Boot ID: 58b4b45d76164318a82bc137f56d45ce
    Virtualization: kvm
  Operating System: Fedora 31 (Thirty One)
       CPE OS Name: cpe:/o:fedoraproject:fedora:31
            Kernel: Linux 5.3.12-300.fc31.x86_64
      Architecture: x86-64
```

### 4. Gestion du réseau (et résolution de noms)

#### NetworkManager

##### lister les interfaces et des informations liées

``` bash
admin in ~ at fedora-tp-leo
➜ nmcli con show
NAME     UUID                                  TYPE      DEVICE  
ens18    c36bf677-f998-341b-8345-bd87869987ba  ethernet  ens18
docker0  3ef273ad-949f-4736-b472-8c5f591a16ed  bridge    docker0

admin in ~ at fedora-tp-leo
➜ nmcli con show ens18
connection.id:                          ens18
connection.uuid:                        c36bf677-f998-341b-8345-bd87869987ba
connection.stable-id:                   --
connection.type:                        802-3-ethernet
connection.interface-name:              ens18
connection.autoconnect:                 yes
connection.autoconnect-priority:        -999
connection.autoconnect-retries:         -1 (default)
connection.multi-connect:               0 (default)
connection.auth-retries:                -1
connection.timestamp:                   1575544893
...
```

##### modifier la configuration réseau d'une interface

``` bash
admin in ~ at fedora-tp-leo
➜ sudo nano /etc/sysconfig/network-scripts/ifcfg-ens18

admin in ~ at fedora-tp-leo took 43s
➜ sudo systemctl reload NetworkManager

admin in ~ at fedora-tp-leo
➜ sudo nmcli con up ens18
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)
```

##### 🌞 afficher les informations DHCP récupérées par NetworkManager (sur une interface en DHCP)

``` bash
admin in ~ at fedora-tp-leo
➜ nmcli connection show ens18 | grep dhcp
ipv4.dhcp-client-id:                    --
ipv4.dhcp-timeout:                      0 (default)
ipv4.dhcp-send-hostname:                yes
ipv4.dhcp-hostname:                     --
ipv4.dhcp-fqdn:                         --
ipv6.dhcp-duid:                         --
ipv6.dhcp-send-hostname:                yes
ipv6.dhcp-hostname:                     --
DHCP4.OPTION[1]:                        dhcp_lease_time = 7200
DHCP4.OPTION[2]:                        dhcp_rebinding_time = 6300
DHCP4.OPTION[3]:                        dhcp_renewal_time = 3600
DHCP4.OPTION[4]:                        dhcp_server_identifier = 192.168.10.254
DHCP4.OPTION[11]:                       requested_dhcp_server_identifier = 1
```

#### `systemd-networkd`

##### 🌞 stopper et désactiver le démarrage de `NetworkManager`

``` bash
admin in ~ at fedora-tp-leo
➜ sudo systemctl stop NetworkManager

admin in ~ at fedora-tp-leo
➜ sudo systemctl disable NetworkManager
Removed /etc/systemd/system/multi-user.target.wants/NetworkManager.service.
Removed /etc/systemd/system/dbus-org.freedesktop.nm-dispatcher.service.
Removed /etc/systemd/system/network-online.target.wants/NetworkManager-wait-online.service.
```

##### 🌞 démarrer et activer le démarrage de `systemd-networkd`

``` bash
admin in ~ at fedora-tp-leo
➜ sudo systemctl start systemd-networkd

admin in ~ at fedora-tp-leo
➜ sudo systemctl enable systemd-networkd
Created symlink /etc/systemd/system/dbus-org.freedesktop.network1.service → /usr/lib/systemd/system/systemd-networkd.service.
Created symlink /etc/systemd/system/multi-user.target.wants/systemd-networkd.service → /usr/lib/systemd/system/systemd-networkd.service.
Created symlink /etc/systemd/system/sockets.target.wants/systemd-networkd.socket → /usr/lib/systemd/system/systemd-networkd.socket.
Created symlink /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service → /usr/lib/systemd/system/systemd-networkd-wait-online.service.
```

##### 🌞 éditer la configuration d'une carte réseau de la VM avec un fichier `.network`

``` bash
admin in ~ at fedora-tp-leo
➜ sudo nano /etc/systemd/network/20-wired.network

admin in ~ at fedora-tp-leo took 8s
➜ cat /etc/systemd/network/20-wired.network
[Match]
Name=ens18

[Network]
DHCP=ipv4
UseHostname=false
LinkLocalAddressing=no
IPv6AcceptRA=no

admin in ~ at fedora-tp-leo
➜ sudo systemctl restart systemd-networkd  

admin in ~ at fedora-tp-leo
➜ networkctl
IDX LINK    TYPE     OPERATIONAL SETUP
  1 lo      loopback carrier     unmanaged
  2 ens18   ether    routable    configured
  3 docker0 bridge   no-carrier  unmanaged

3 links listed.

admin in ~ at fedora-tp-leo
➜ sudo systemctl restart network-online.target

admin in ~ at fedora-tp-leo
➜ sudo systemctl status network-online.target
● network-online.target - Network is Online
   Loaded: loaded (/usr/lib/systemd/system/network-online.target; static; vendor p>
   Active: active since Thu 2019-12-05 11:41:29 UTC; 5s ago
     Docs: man:systemd.special(7)
           https://www.freedesktop.org/wiki/Software/systemd/NetworkTarget

Dec 05 11:41:29 fedora-tp systemd[1]: Reached target Network is Online.
```

#### `systemd-resolved`

##### 🌞 activer la résolution de noms par `systemd-resolved` en démarrant le service (maintenant et au boot)

``` bash
admin in ~ at fedora-tp
➜ sudo systemctl start systemd-resolved

admin in ~ at fedora-tp
➜ sudo systemctl enable systemd-resolved
Created symlink /etc/systemd/system/dbus-org.freedesktop.resolve1.service → /usr/lib/systemd/system/systemd-resolved.service.
Created symlink /etc/systemd/system/multi-user.target.wants/systemd-resolved.service → /usr/lib/systemd/system/systemd-resolved.service.

admin in ~ at fedora-tp
➜ sudo rm -rf /etc/resolv.conf

admin in ~ at fedora-tp
admin in ~ at fedora-tp
➜ sudo ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
```

###### 🌞 vérifier que le service est lancé

``` bash
admin in ~ at fedora-tp
➜ systemctl status systemd-resolved.service
● systemd-resolved.service - Network Name Resolution
   Loaded: loaded (/usr/lib/systemd/system/systemd-resolved.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2019-12-05 11:52:59 UTC; 40s ago
     Docs: man:systemd-resolved.service(8)
           https://www.freedesktop.org/wiki/Software/systemd/resolved
           https://www.freedesktop.org/wiki/Software/systemd/writing-network-configuration-managers
           https://www.freedesktop.org/wiki/Software/systemd/writing-resolver-clients
 Main PID: 13677 (systemd-resolve)
   Status: "Processing requests..."
    Tasks: 1 (limit: 4688)
   Memory: 11.3M
   CGroup: /system.slice/systemd-resolved.service
           └─13677 /usr/lib/systemd/systemd-resolved

Dec 05 11:52:59 fedora-tp systemd[1]: Starting Network Name Resolution...
Dec 05 11:52:59 fedora-tp systemd-resolved[13677]: Positive Trust Anchors:
Dec 05 11:52:59 fedora-tp systemd-resolved[13677]: . IN DS 19036 8 2 49aac11d7b6f6446702e54a1607371607a1a41855200fd2ce1cdde32f24e8fb5
Dec 05 11:52:59 fedora-tp systemd-resolved[13677]: . IN DS 20326 8 2 e06d44b80b8f1d39a95c0b0d7c65d08458e880409bbc683457104237c7f8ec8d
Dec 05 11:52:59 fedora-tp systemd-resolved[13677]: Negative trust anchors: 10.in-addr.arpa 16.172.in-addr.arpa 17.172.in-addr.arpa 18.172.in-addr.arpa 19.172.in-addr.ar>
Dec 05 11:52:59 fedora-tp systemd-resolved[13677]: Using system hostname 'fedora-tp'.
Dec 05 11:52:59 fedora-tp systemd[1]: Started Network Name Resolution.
```

##### 🌞 vérifier qu'un serveur DNS tourne localement et écoute sur un port de l'interface localhost (avec `ss` par exemple)

``` bash
admin in ~ at fedora-tp
➜ sudo ss -l -p | grep resolve
nl                 UNCONN              0                    0                                                                                              rtnl:systemd-resolve/13677                                       *                                                                                                   
nl                 UNCONN              0                    0                                                                                              rtnl:systemd-resolve/13677                                       *                                                                                                   
u_dgr              UNCONN              0                    0                                                                                                 * 51160                                                      * 17445               users:(("systemd-resolve",pid=13677,fd=3))                                     
udp                UNCONN              0                    0                                                                                           0.0.0.0:hostmon                                              0.0.0.0:*                   users:(("systemd-resolve",pid=13677,fd=12))                                    
udp                UNCONN              0                    0                                                                                     127.0.0.53%lo:domain                                               0.0.0.0:*                   users:(("systemd-resolve",pid=13677,fd=18))                                    
udp                UNCONN              0                    0                                                                                              [::]:hostmon                                                 [::]:*                   users:(("systemd-resolve",pid=13677,fd=14))                                    
tcp                LISTEN              0                    128                                                                                         0.0.0.0:hostmon                                              0.0.0.0:*                   users:(("systemd-resolve",pid=13677,fd=13))                                    
tcp                LISTEN              0                    128                                                                                   127.0.0.53%lo:domain                                               0.0.0.0:*                   users:(("systemd-resolve",pid=13677,fd=19))                                    
tcp                LISTEN              0                    128                                                                                            [::]:hostmon
```

###### 🌞 effectuer une commande de résolution de nom en utilisant explicitement le serveur DNS mis en place par `systemd-resolved` (avec `dig`)

``` bash
admin in ~ at fedora-tp
➜ dig @127.0.0.53 +short cyber-stuff.net
62.210.5.33
```

###### 🌞 effectuer une requête DNS avec `systemd-resolve`

``` bash
admin in ~ at fedora-tp
➜ systemd-resolve l42.fr
l42.fr: 62.210.5.33                            -- link: ens18

-- Information acquired via protocol DNS in 1.2ms.
-- Data is authenticated: yes
```

##### on peut utiliser `resolvectl` pour avoir des infos sur le serveur local

``` bash
admin in ~ at fedora-tp 
➜ resolvectl
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
  Current DNS Server: 1.1.1.1
         DNS Servers: 1.1.1.1
                      1.0.0.1
Fallback DNS Servers: 1.1.1.1
                      8.8.8.8
                      1.0.0.1
                      8.8.4.4
...
```

##### 🌞 Afin d'activer de façon permanente ce serveur DNS, la bonne pratique est de remplacer `/etc/resolv.conf` par un lien symbolique pointant vers `/run/systemd/resolve/stub-resolv.conf`

``` bash
Déja fait avant :O Quelle vie !
```

##### 🌞 Modifier la configuration de `systemd-resolved`

- ajouter les serveurs de votre choix
- vérifier la modification avec `resolvectl`

``` bash
admin in ~ at fedora-tp
➜ sudo nano /etc/systemd/resolved.conf

admin in ~ at fedora-tp took 8s
➜ cat /etc/systemd/resolved.conf
[Resolve]
#DNS=
FallbackDNS=1.0.0.1
#Domains=
LLMNR=no
MulticastDNS=no
#DNSSEC=allow-downgrade
#DNSOverTLS=no
Cache=yes
DNSStubListener=yes
ReadEtcHosts=yes

admin in ~ at fedora-tp
➜ sudo systemctl restart systemd-resolved

admin in ~ at fedora-tp
➜ resolvectl
Global
       LLMNR setting: no
MulticastDNS setting: no
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
Fallback DNS Servers: 1.0.0.1
...
```

##### 🌞 mise en place de DNS over TLS

###### renseignez-vous sur les avantages de DNS over TLS

Cela améliore la confidentialité des échanges entre l'utilisateur et les serveurs cibles en obfuscant les demandes de correspondances DNS et cela protège contre les attaques Man in the Middle.

###### effectuer une configuration globale (dans `/etc/systemd/resolved.conf`)

- compléter la clause `DNS` pour ajouter un serveur qui supporte le DNS over TLS (on peut en trouver des listes sur internet)
- utiliser la clause `DNSOverTLS` pour activer la fonctionnalité
  - valeur `opportunistic` pour tester les résolutions à travers TLS, et fallback sur une résolution DNS classique en cas d'erreur
  - valeur `yes` pour forcer les résolutions à travers TLS

``` bash
admin in ~ at fedora-tp
➜ sudo nano /etc/systemd/resolved.conf

admin in ~ at fedora-tp took 31s
➜ cat /etc/systemd/resolved.conf
[Resolve]
DNS=1.1.1.1
FallbackDNS=1.0.0.1
#Domains=
LLMNR=no
MulticastDNS=no
#DNSSEC=allow-downgrade
DNSOverTLS=yes
Cache=yes
DNSStubListener=yes
ReadEtcHosts=yes

admin in ~ at fedora-tp
➜ sudo systemctl restart systemd-resolved

admin in ~ at fedora-tp
➜ resolvectl
Global
       LLMNR setting: no
MulticastDNS setting: no
  DNSOverTLS setting: yes
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
         DNS Servers: 1.1.1.1
Fallback DNS Servers: 1.0.0.1
```

###### prouver avec `tcpdump` que les résolutions sont bien à travers TLS (les serveurs DNS qui supportent le DNS over TLS écoutent sur le port 853)

``` bash
admin in ~ at fedora-tp
➜ sudo tcpdump port 853
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on ens18, link-type EN10MB (Ethernet), capture size 262144 bytes
14:07:14.445329 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [S], seq 3790444845:3790445471, win 64240, options [mss 1460,sackOK,TS val 4017367871 ecr 0,nop,wscale 7,tfo  cookie 31f8104494e6c638,nop,nop], length 626
14:07:14.446709 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [S.], seq 695444846, ack 3790444846, win 29200, options [mss 1460,nop,nop,sackOK,nop,wscale 10], length 0
14:07:14.446744 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [P.], seq 1:627, ack 1, win 502, length 626
14:07:14.447880 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [.], ack 627, win 30, length 0
14:07:14.448294 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [P.], seq 1:2676, ack 627, win 30, length 2675
14:07:14.448312 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [.], ack 2676, win 496, length 0
14:07:14.459829 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [P.], seq 627:753, ack 2676, win 501, length 126
14:07:14.459953 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [P.], seq 753:784, ack 2676, win 501, length 31
14:07:14.460027 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [P.], seq 784:880, ack 2676, win 501, length 96
14:07:14.461626 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [P.], seq 2676:2934, ack 880, win 30, length 258
14:07:14.461675 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [.], ack 2934, win 501, length 0
14:07:14.479877 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [P.], seq 2934:3200, ack 880, win 30, length 266
14:07:14.479927 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [.], ack 3200, win 501, length 0
14:07:24.479529 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [P.], seq 3200:3231, ack 880, win 30, length 31
14:07:24.479575 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [.], ack 3231, win 501, length 0
14:07:24.479651 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [F.], seq 3231, ack 880, win 30, length 0
14:07:24.479840 IP fedora-tp.49814 > one.one.one.one.domain-s: Flags [F.], seq 880, ack 3232, win 501, length 0
14:07:24.481002 IP one.one.one.one.domain-s > fedora-tp.49814: Flags [.], ack 881, win 30, length 0
```

##### 🌞 activer l'utilisation de DNSSEC

``` bash
admin in ~ at fedora-tp
➜ sudo nano /etc/systemd/resolved.conf

admin in ~ at fedora-tp
➜ sudo systemctl restart systemd-resolved

admin in ~ at fedora-tp
➜ cat /etc/systemd/resolved.conf
[Resolve]
DNS=1.1.1.1
FallbackDNS=1.0.0.1
#Domains=
LLMNR=no
MulticastDNS=no
DNSSEC=yes
DNSOverTLS=yes
Cache=yes
DNSStubListener=yes
ReadEtcHosts=yes
```

### 5. Gestion de sessions `logind`

``` bash
admin in ~ at fedora-tp
➜ loginctl
activate           -- Activate a session
attach             -- Attach one or more devices to a seat
disable-linger     -- Disable linger state of one or more users
enable-linger      -- Enable linger state of one or more users
flush-devices      -- Flush all device associations
kill-session       -- Send signal to processes of a session
kill-user          -- Send signal to processes of a user
list-seats         -- List seats
list-sessions      -- List sessions
list-users         -- List users
lock-session       -- Screen lock one or more sessions
lock-sessions      -- Screen lock all current sessions
seat-status        -- Show seat status
session-status     -- Show session status
show-seat          -- Show properties of one or more seats
show-session       -- Show properties of one or more sessions
show-user          -- Show properties of one or more users
terminate-seat     -- Terminate all sessions on one or more seats
terminate-session  -- Terminate one or more sessions
terminate-user     -- Terminate all sessions of one or more users
unlock-session     -- Screen unlock one or more sessions
unlock-sessions    -- Screen unlock all current sessions
user-status        -- Show user status
```

### 6. Gestion d'unité basique (services)

#### lister les unités `systemd` actives de la machine

``` bash
admin in ~ at fedora-tp
➜ systemctl list-units -t service
  UNIT                                 LOAD   ACTIVE SUB     DESCRIPTION                                                                  
  auditd.service                       loaded active running Security Auditing Service                                                    
  containerd.service                   loaded active running containerd container runtime                                                 
  dbus-broker.service                  loaded active running D-Bus System Message Bus                                                     
  docker.service                       loaded active running Docker Application Container Engine                                          
  dracut-shutdown.service              loaded active exited  Restore /run/initramfs on shutdown                                           
  firewalld.service                    loaded active running firewalld - dynamic firewall daemon                                          
  getty@tty1.service                   loaded active running Getty on tty1                                                                
  kmod-static-nodes.service            loaded active exited  Create list of static device nodes for the current kernel                    
  lvm2-monitor.service                 loaded active exited  Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling
  lvm2-pvscan@8:2.service              loaded active exited  LVM event activation on device 8:2                                           
● NetworkManager-wait-online.service   loaded failed failed  Network Manager Wait Online                                                  
  sshd.service                         loaded active running OpenSSH server daemon                                                        
  sssd.service                         loaded active running System Security Services Daemon                                              
  systemd-journal-flush.service        loaded active exited  Flush Journal to Persistent Storage                                          
  systemd-journald.service             loaded active running Journal Service                                                              
  systemd-logind.service               loaded active running Login Service                                                                
  systemd-networkd-wait-online.service loaded active exited  Wait for Network to be Configured                                            
  systemd-networkd.service             loaded active running Network Service                                                              
  systemd-random-seed.service          loaded active exited  Load/Save Random Seed                                                        
  systemd-remount-fs.service           loaded active exited  Remount Root and Kernel File Systems                                         
  systemd-resolved.service             loaded active running Network Name Resolution                                                      
  systemd-sysctl.service               loaded active exited  Apply Kernel Variables                                                       
  systemd-tmpfiles-setup-dev.service   loaded active exited  Create Static Device Nodes in /dev                                           
  systemd-tmpfiles-setup.service       loaded active exited  Create Volatile Files and Directories                                        
  systemd-udev-trigger.service         loaded active exited  udev Coldplug all Devices                                                    
  systemd-udevd.service                loaded active running udev Kernel Device Manager                                                   
  systemd-update-utmp.service          loaded active exited  Update UTMP about System Boot/Shutdown                                       
  systemd-user-sessions.service        loaded active exited  Permit User Sessions                                                         
  user-runtime-dir@1000.service        loaded active exited  User Runtime Directory /run/user/1000                                        
  user@1000.service                    loaded active running User Manager for UID 1000                                                    

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.

30 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.
```

#### 🌞 trouver l'unité associée au processus `chronyd`

``` bash
admin in ~ at fedora-tp
➜ ps -e -o pid,cmd,unit | grep chronyd
  20830 /usr/sbin/chronyd           chronyd.service
```

## II. Boot et Logs

### Boot

#### 🌞 générer un graphe de la séquence de boot

``` bash
admin in ~ at fedora-tp
➜ systemd-analyze plot > graphe.svg

admin in ~ at fedora-tp
➜ cat graphe.svg
Mec, désactive NetworkManager.
```

##### 🌞 déterminer le temps qu'a mis `sshd.service` à démarrer

``` bash
admin in ~ at fedora-tp took 4s
➜ systemd-analyze blame | grep sshd
     47.338s sshd.service
```

### Logs

``` bash
admin in ~ at fedora-tp
➜ sudo journalctl -xe
-- A start job for unit session-21.scope has finished successfully.
--
-- The job identifier is 4741.
Dec 05 14:28:57 fedora-tp sshd[22066]: pam_unix(sshd:session): session opened for >
Dec 05 14:28:57 fedora-tp audit[22066]: USER_START pid=22066 uid=0 auid=1000 ses=2>
Dec 05 14:28:57 fedora-tp audit[22069]: CRYPTO_KEY_USER pid=22069 uid=0 auid=1000 >
Dec 05 14:28:57 fedora-tp audit[22069]: CRED_ACQ pid=22069 uid=0 auid=1000 ses=21 >
Dec 05 14:28:57 fedora-tp audit[22066]: USER_LOGIN pid=22066 uid=0 auid=1000 ses=2>
Dec 05 14:28:57 fedora-tp audit[22066]: USER_START pid=22066 uid=0 auid=1000 ses=2>
Dec 05 14:28:57 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 >
Dec 05 14:28:58 fedora-tp sshd[22069]: Received disconnect from 192.168.9.21 port >
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 >
Dec 05 14:28:58 fedora-tp sshd[22069]: Disconnected from user admin 192.168.9.21 p>
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 >
Dec 05 14:28:58 fedora-tp sshd[22066]: pam_unix(sshd:session): session closed for >
Dec 05 14:28:58 fedora-tp audit[22066]: USER_END pid=22066 uid=0 auid=1000 ses=21 >
Dec 05 14:28:58 fedora-tp audit[22066]: CRED_DISP pid=22066 uid=0 auid=1000 ses=21>
Dec 05 14:28:58 fedora-tp audit[22066]: USER_END pid=22066 uid=0 auid=1000 ses=21 >
Dec 05 14:28:58 fedora-tp audit[22066]: USER_LOGOUT pid=22066 uid=0 auid=1000 ses=>
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 >
Dec 05 14:28:58 fedora-tp systemd[1]: session-21.scope: Succeeded.
-- Subject: Unit succeeded
-- Defined-By: systemd
-- Support: https://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- The unit session-21.scope has successfully entered the 'dead' state.
Dec 05 14:28:58 fedora-tp systemd-logind[741]: Session 21 logged out. Waiting for >
Dec 05 14:28:58 fedora-tp systemd-logind[741]: Removed session 21.
-- Subject: Session 21 has been terminated
-- Defined-By: systemd
-- Support: https://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- Documentation: https://www.freedesktop.org/wiki/Software/systemd/multiseat
-- 
-- A session with the ID 21 has been terminated.
Dec 05 14:42:43 fedora-tp audit[22374]: USER_AUTH pid=22374 uid=1000 auid=1000 ses>
Dec 05 14:42:43 fedora-tp audit[22374]: USER_ACCT pid=22374 uid=1000 auid=1000 ses>
Dec 05 14:42:43 fedora-tp sudo[22374]:    admin : TTY=pts/0 ; PWD=/home/admin ; US>
Dec 05 14:42:43 fedora-tp audit[22374]: USER_CMD pid=22374 uid=1000 auid=1000 ses=>
Dec 05 14:42:43 fedora-tp audit[22374]: CRED_REFR pid=22374 uid=0 auid=1000 ses=4 >
Dec 05 14:42:43 fedora-tp sudo[22374]: pam_unix(sudo:session): session opened for >
Dec 05 14:42:43 fedora-tp audit[22374]: USER_START pid=22374 uid=0 auid=1000 ses=4>
lines 1761-1801/1801 (END)
-- A start job for unit session-21.scope has finished successfully.
-- 
-- The job identifier is 4741.
Dec 05 14:28:57 fedora-tp sshd[22066]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:57 fedora-tp audit[22066]: USER_START pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:session_open grantors=pam_>
Dec 05 14:28:57 fedora-tp audit[22069]: CRYPTO_KEY_USER pid=22069 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=server fp=SH>
Dec 05 14:28:57 fedora-tp audit[22069]: CRED_ACQ pid=22069 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:setcred grantors=pam_env,pam>
Dec 05 14:28:57 fedora-tp audit[22066]: USER_LOGIN pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/s>
Dec 05 14:28:57 fedora-tp audit[22066]: USER_START pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/s>
Dec 05 14:28:57 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=server fp=SH>
Dec 05 14:28:58 fedora-tp sshd[22069]: Received disconnect from 192.168.9.21 port 47254:11: disconnected by user
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=session fp=?>
Dec 05 14:28:58 fedora-tp sshd[22069]: Disconnected from user admin 192.168.9.21 port 47254
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=server fp=SH>
Dec 05 14:28:58 fedora-tp sshd[22066]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:58 fedora-tp audit[22066]: USER_END pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:session_close grantors=pam_s>
Dec 05 14:28:58 fedora-tp audit[22066]: CRED_DISP pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:setcred grantors=pam_env,pa>
Dec 05 14:28:58 fedora-tp audit[22066]: USER_END pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/ssh>
Dec 05 14:28:58 fedora-tp audit[22066]: USER_LOGOUT pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/>
lines 1761-1779/1801 99%
-- A start job for unit session-21.scope has finished successfully.
-- 
-- The job identifier is 4741.
Dec 05 14:28:57 fedora-tp sshd[22066]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:57 fedora-tp audit[22066]: USER_START pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:session_open grantors=pam_s>
Dec 05 14:28:57 fedora-tp audit[22069]: CRYPTO_KEY_USER pid=22069 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=server fp=SHA>
Dec 05 14:28:57 fedora-tp audit[22069]: CRED_ACQ pid=22069 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:setcred grantors=pam_env,pam_>
Dec 05 14:28:57 fedora-tp audit[22066]: USER_LOGIN pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/ss>
Dec 05 14:28:57 fedora-tp audit[22066]: USER_START pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/ss>
Dec 05 14:28:57 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=server fp=SHA>
Dec 05 14:28:58 fedora-tp sshd[22069]: Received disconnect from 192.168.9.21 port 47254:11: disconnected by user
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=session fp=? >
Dec 05 14:28:58 fedora-tp sshd[22069]: Disconnected from user admin 192.168.9.21 port 47254
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=server fp=SHA>
Dec 05 14:28:58 fedora-tp sshd[22066]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:58 fedora-tp audit[22066]: USER_END pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:session_close grantors=pam_se>
Dec 05 14:28:58 fedora-tp audit[22066]: CRED_DISP pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=PAM:setcred grantors=pam_env,pam>
Dec 05 14:28:58 fedora-tp audit[22066]: USER_END pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/sshd>
Dec 05 14:28:58 fedora-tp audit[22066]: USER_LOGOUT pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=login id=1000 exe="/usr/sbin/s>
Dec 05 14:28:58 fedora-tp audit[22066]: CRYPTO_KEY_USER pid=22066 uid=0 auid=1000 ses=21 subj=system_u:system_r:sshd_t:s0-s0:c0.c1023 msg='op=destroy kind=server fp=SHA>
Dec 05 14:28:58 fedora-tp systemd[1]: session-21.scope: Succeeded.
-- Subject: Unit succeeded
-- Defined-By: systemd
-- Support: https://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- The unit session-21.scope has successfully entered the 'dead' state.
Dec 05 14:28:58 fedora-tp systemd-logind[741]: Session 21 logged out. Waiting for processes to exit.
Dec 05 14:28:58 fedora-tp systemd-logind[741]: Removed session 21.
-- Subject: Session 21 has been terminated
-- Defined-By: systemd
-- Support: https://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- Documentation: https://www.freedesktop.org/wiki/Software/systemd/multiseat
-- 
-- A session with the ID 21 has been terminated.
Dec 05 14:42:43 fedora-tp audit[22374]: USER_AUTH pid=22374 uid=1000 auid=1000 ses=4 subj=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 msg='op=PAM:authenticati>
Dec 05 14:42:43 fedora-tp audit[22374]: USER_ACCT pid=22374 uid=1000 auid=1000 ses=4 subj=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 msg='op=PAM:accounting g>
Dec 05 14:42:43 fedora-tp sudo[22374]:    admin : TTY=pts/0 ; PWD=/home/admin ; USER=root ; COMMAND=/usr/bin/journalctl -xe
Dec 05 14:42:43 fedora-tp audit[22374]: USER_CMD pid=22374 uid=1000 auid=1000 ses=4 subj=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 msg='cwd="/home/admin" cm>
Dec 05 14:42:43 fedora-tp audit[22374]: CRED_REFR pid=22374 uid=0 auid=1000 ses=4 subj=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 msg='op=PAM:setcred grantor>
Dec 05 14:42:43 fedora-tp sudo[22374]: pam_unix(sudo:session): session opened for user root by admin(uid=0)
Dec 05 14:42:43 fedora-tp audit[22374]: USER_START pid=22374 uid=0 auid=1000 ses=4 subj=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 msg='op=PAM:session_open g>
...

admin in ~ at fedora-tp took 3s 
➜ sudo journalctl -k 
-- Logs begin at Fri 2019-11-29 10:34:51 UTC, end at Thu 2019-12-05 14:43:45 UTC. --
Dec 05 09:48:28 fedora-tp-leo kernel: Linux version 5.3.12-300.fc31.x86_64 (mockbuild@bkernel04.phx2.fedoraproject.org) (gcc version 9.2.1 20190827 (Red Hat 9.2.1-1) (G>
Dec 05 09:48:28 fedora-tp-leo kernel: Command line: BOOT_IMAGE=(hd0,msdos1)/vmlinuz-5.3.12-300.fc31.x86_64 root=/dev/mapper/fedora_fedora--tp--leo-root ro systemd.unifi>
Dec 05 09:48:28 fedora-tp-leo kernel: x86/fpu: x87 FPU will use FXSAVE
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-provided physical RAM map:
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x0000000000000000-0x000000000009fbff] usable
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x00000000000f0000-0x00000000000fffff] reserved
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x0000000000100000-0x00000000bffdafff] usable
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x00000000bffdb000-0x00000000bfffffff] reserved
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x00000000feffc000-0x00000000feffffff] reserved
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x00000000fffc0000-0x00000000ffffffff] reserved
Dec 05 09:48:28 fedora-tp-leo kernel: BIOS-e820: [mem 0x0000000100000000-0x000000013fffffff] usable
Dec 05 09:48:28 fedora-tp-leo kernel: NX (Execute Disable) protection: active
Dec 05 09:48:28 fedora-tp-leo kernel: SMBIOS 2.8 present.
Dec 05 09:48:28 fedora-tp-leo kernel: DMI: QEMU Standard PC (i440FX + PIIX, 1996), BIOS rel-1.12.1-0-ga5cab58e9a3f-prebuilt.qemu.org 04/01/2014
Dec 05 09:48:28 fedora-tp-leo kernel: Hypervisor detected: KVM
Dec 05 09:48:28 fedora-tp-leo kernel: kvm-clock: Using msrs 4b564d01 and 4b564d00
Dec 05 09:48:28 fedora-tp-leo kernel: kvm-clock: cpu 0, msr 39e01001, primary cpu clock
Dec 05 09:48:28 fedora-tp-leo kernel: kvm-clock: using sched offset of 515625653389775 cycles
Dec 05 09:48:28 fedora-tp-leo kernel: clocksource: kvm-clock: mask: 0xffffffffffffffff max_cycles: 0x1cd42e4dffb, max_idle_ns: 881590591483 ns
Dec 05 09:48:28 fedora-tp-leo kernel: tsc: Detected 2199.996 MHz processor
Dec 05 09:48:28 fedora-tp-leo kernel: e820: update [mem 0x00000000-0x00000fff] usable ==> reserved
Dec 05 09:48:28 fedora-tp-leo kernel: e820: remove [mem 0x000a0000-0x000fffff] usable
Dec 05 09:48:28 fedora-tp-leo kernel: last_pfn = 0x140000 max_arch_pfn = 0x400000000
Dec 05 09:48:28 fedora-tp-leo kernel: MTRR default type: write-back
Dec 05 09:48:28 fedora-tp-leo kernel: MTRR fixed ranges enabled:
Dec 05 09:48:28 fedora-tp-leo kernel:   00000-9FFFF write-back
Dec 05 09:48:28 fedora-tp-leo kernel:   A0000-BFFFF uncachable
Dec 05 09:48:28 fedora-tp-leo kernel:   C0000-FFFFF write-protect
Dec 05 09:48:28 fedora-tp-leo kernel: MTRR variable ranges enabled:
Dec 05 09:48:28 fedora-tp-leo kernel:   0 base 00C0000000 mask FFC0000000 uncachable
Dec 05 09:48:28 fedora-tp-leo kernel:   1 disabled
Dec 05 09:48:28 fedora-tp-leo kernel:   2 disabled
Dec 05 09:48:28 fedora-tp-leo kernel:   3 disabled
Dec 05 09:48:28 fedora-tp-leo kernel:   4 disabled
Dec 05 09:48:28 fedora-tp-leo kernel:   5 disabled
Dec 05 09:48:28 fedora-tp-leo kernel:   6 disabled
Dec 05 09:48:28 fedora-tp-leo kernel:   7 disabled
Dec 05 09:48:28 fedora-tp-leo kernel: x86/PAT: Configuration [0-7]: WB  WC  UC- UC  WB  WC  UC- UC  
Dec 05 09:48:28 fedora-tp-leo kernel: last_pfn = 0xbffdb max_arch_pfn = 0x400000000
...

admin in ~ at fedora-tp 
➜ sudo journalctl -xe -u sshd.service
Dec 05 14:28:43 fedora-tp sshd[21976]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:43 fedora-tp sshd[21976]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:46 fedora-tp sshd[21981]: Accepted publickey for admin from 192.168.9.21 port 47228 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:46 fedora-tp sshd[21981]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:46 fedora-tp sshd[21981]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:47 fedora-tp sshd[21986]: Accepted publickey for admin from 192.168.9.21 port 47230 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:47 fedora-tp sshd[21986]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:47 fedora-tp sshd[21986]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:47 fedora-tp sshd[21991]: Accepted publickey for admin from 192.168.9.21 port 47232 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:47 fedora-tp sshd[21991]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:47 fedora-tp sshd[21991]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:48 fedora-tp sshd[21996]: Accepted publickey for admin from 192.168.9.21 port 47234 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:48 fedora-tp sshd[21996]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:48 fedora-tp sshd[21996]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:48 fedora-tp sshd[22001]: Accepted publickey for admin from 192.168.9.21 port 47236 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:48 fedora-tp sshd[22001]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:48 fedora-tp sshd[22001]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:49 fedora-tp sshd[22006]: Accepted publickey for admin from 192.168.9.21 port 47238 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:49 fedora-tp sshd[22006]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:49 fedora-tp sshd[22006]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:49 fedora-tp sshd[22011]: Accepted publickey for admin from 192.168.9.21 port 47240 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:49 fedora-tp sshd[22011]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:49 fedora-tp sshd[22011]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:50 fedora-tp sshd[22016]: Accepted publickey for admin from 192.168.9.21 port 47242 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:50 fedora-tp sshd[22016]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:50 fedora-tp sshd[22016]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:50 fedora-tp sshd[22021]: Accepted publickey for admin from 192.168.9.21 port 47244 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:50 fedora-tp sshd[22021]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:50 fedora-tp sshd[22021]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:51 fedora-tp sshd[22051]: Accepted publickey for admin from 192.168.9.21 port 47246 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:51 fedora-tp sshd[22051]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:51 fedora-tp sshd[22051]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:51 fedora-tp sshd[22056]: Accepted publickey for admin from 192.168.9.21 port 47248 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:51 fedora-tp sshd[22056]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:51 fedora-tp sshd[22056]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:52 fedora-tp sshd[22061]: Accepted publickey for admin from 192.168.9.21 port 47250 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:52 fedora-tp sshd[22061]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:52 fedora-tp sshd[22061]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:57 fedora-tp sshd[22066]: Accepted publickey for admin from 192.168.9.21 port 47254 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:57 fedora-tp sshd[22066]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:58 fedora-tp sshd[22066]: pam_unix(sshd:session): session closed for user admin
...

admin in ~ at fedora-tp 
➜ sudo journalctl -xe -u sshd.service --since yesterday
Dec 05 14:28:43 fedora-tp sshd[21976]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:43 fedora-tp sshd[21976]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:46 fedora-tp sshd[21981]: Accepted publickey for admin from 192.168.9.21 port 47228 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:46 fedora-tp sshd[21981]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:46 fedora-tp sshd[21981]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:47 fedora-tp sshd[21986]: Accepted publickey for admin from 192.168.9.21 port 47230 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:47 fedora-tp sshd[21986]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:47 fedora-tp sshd[21986]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:47 fedora-tp sshd[21991]: Accepted publickey for admin from 192.168.9.21 port 47232 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:47 fedora-tp sshd[21991]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:47 fedora-tp sshd[21991]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:48 fedora-tp sshd[21996]: Accepted publickey for admin from 192.168.9.21 port 47234 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:48 fedora-tp sshd[21996]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:48 fedora-tp sshd[21996]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:48 fedora-tp sshd[22001]: Accepted publickey for admin from 192.168.9.21 port 47236 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:48 fedora-tp sshd[22001]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:48 fedora-tp sshd[22001]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:49 fedora-tp sshd[22006]: Accepted publickey for admin from 192.168.9.21 port 47238 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:49 fedora-tp sshd[22006]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:49 fedora-tp sshd[22006]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:49 fedora-tp sshd[22011]: Accepted publickey for admin from 192.168.9.21 port 47240 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:49 fedora-tp sshd[22011]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:49 fedora-tp sshd[22011]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:50 fedora-tp sshd[22016]: Accepted publickey for admin from 192.168.9.21 port 47242 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:50 fedora-tp sshd[22016]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:50 fedora-tp sshd[22016]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:50 fedora-tp sshd[22021]: Accepted publickey for admin from 192.168.9.21 port 47244 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:50 fedora-tp sshd[22021]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:50 fedora-tp sshd[22021]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:51 fedora-tp sshd[22051]: Accepted publickey for admin from 192.168.9.21 port 47246 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:51 fedora-tp sshd[22051]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:51 fedora-tp sshd[22051]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:51 fedora-tp sshd[22056]: Accepted publickey for admin from 192.168.9.21 port 47248 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:51 fedora-tp sshd[22056]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:51 fedora-tp sshd[22056]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:52 fedora-tp sshd[22061]: Accepted publickey for admin from 192.168.9.21 port 47250 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:52 fedora-tp sshd[22061]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:52 fedora-tp sshd[22061]: pam_unix(sshd:session): session closed for user admin
Dec 05 14:28:57 fedora-tp sshd[22066]: Accepted publickey for admin from 192.168.9.21 port 47254 ssh2: ED25519 SHA256:juziOONNlvk+lSHLMbP7RySveVysNmmYQ8omRx3zwnI
Dec 05 14:28:57 fedora-tp sshd[22066]: pam_unix(sshd:session): session opened for user admin by (uid=0)
Dec 05 14:28:58 fedora-tp sshd[22066]: pam_unix(sshd:session): session closed for user admin
...

admin in ~ at fedora-tp 
➜ sudo journalctl -xe -u sshd.service  --since "2019-11-28 01:00:00" --output=json
{"_SYSTEMD_SLICE":"system.slice","__MONOTONIC_TIMESTAMP":"16817038367","SYSLOG_FACILITY":"10","_SYSTEMD_UNIT":"sshd.service","_SYSTEMD_CGROUP":"/system.slice/sshd.servi>
{"_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=2251;b=58b4b45d76164318a82bc137f56d45ce;m=3ea616b25;t=598f5bf0d0109;x=af>
{"_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","SYSLOG_IDENTIFIER":"sshd","__REALTIME_TIMESTAMP":"1575556126770932","_CAP_EFFECTIVE":"3fffffffff","MESSAGE":"Accepted pu>
{"SYSLOG_FACILITY":"10","_SYSTEMD_CGROUP":"/system.slice/sshd.service","PRIORITY":"6","_MACHINE_ID":"be1eb5b0480b41e294953b05585e2abd","_HOSTNAME":"fedora-tp","_EXE":"/>
{"_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","SYSLOG_TIMESTAMP":"Dec  5 14:28:46 ","PRIORITY":"6","_HOSTN>
{"__MONOTONIC_TIMESTAMP":"16821126860","SYSLOG_TIMESTAMP":"Dec  5 14:28:47 ","_SYSTEMD_INVOCATION_ID":"8bf7002f4b9143ea9815053c054cd3c3","_CMDLINE":"sshd: admin [priv]">
{"_TRANSPORT":"syslog","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=2288;b=58b4b45d76164318a82bc137f56d45ce;m=3ea9ea6ef;t=598f5bf4a3cd2;x=c225ff80b67711ed","_HOSTNA>
{"PRIORITY":"6","_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","SYSLOG_PID":"21986","_SOURCE_REALTIME_TIMESTAMP":"1575556127393703","SYSLOG_FACILITY":"10">
{"_COMM":"sshd","_HOSTNAME":"fedora-tp","_SYSTEMD_INVOCATION_ID":"8bf7002f4b9143ea9815053c054cd3c3","__MONOTONIC_TIMESTAMP":"16821621197","_CMDLINE":"sshd: admin [priv]>
{"_MACHINE_ID":"be1eb5b0480b41e294953b05585e2abd","_SOURCE_REALTIME_TIMESTAMP":"1575556127768170","_GID":"0","_CMDLINE":"sshd: admin [priv]","_SYSTEMD_SLICE":"system.sl>
{"__MONOTONIC_TIMESTAMP":"16821776526","_SOURCE_REALTIME_TIMESTAMP":"1575556127884893","_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","_UID":"0","SYSLOG_F>
{"__REALTIME_TIMESTAMP":"1575556128207042","_EXE":"/usr/sbin/sshd","_HOSTNAME":"fedora-tp","SYSLOG_IDENTIFIER":"sshd","_PID":"21996","__MONOTONIC_TIMESTAMP":"1682209865>
{"_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=22ca;b=58b4b45d76164318a82bc137f56d45ce;m=3eaad6f8c;t=598>
{"_CAP_EFFECTIVE":"3fffffffff","_SYSTEMD_UNIT":"sshd.service","_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","_TRANSPORT":"syslog","_SOURCE_REALTIME_TIMES>
{"_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","_SYSTEMD_UNIT":"sshd.service","MESSAGE":"Accepted publickey for admin from 192.168.9.21 port 47236 ssh2: >
{"__MONOTONIC_TIMESTAMP":"16822606895","__REALTIME_TIMESTAMP":"1575556128715282","_COMM":"sshd","_SYSTEMD_CGROUP":"/system.slice/sshd.service","_MACHINE_ID":"be1eb5b048>
{"__MONOTONIC_TIMESTAMP":"16822725252","_SYSTEMD_SLICE":"system.slice","_COMM":"sshd","_CMDLINE":"sshd: admin [priv]","SYSLOG_FACILITY":"10","_SYSTEMD_UNIT":"sshd.servi>
{"_TRANSPORT":"syslog","_SYSTEMD_INVOCATION_ID":"8bf7002f4b9143ea9815053c054cd3c3","_HOSTNAME":"fedora-tp","__REALTIME_TIMESTAMP":"1575556129137264","_SYSTEMD_CGROUP":">
{"_SYSTEMD_SLICE":"system.slice","SYSLOG_IDENTIFIER":"sshd","_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","_HOSTNAME":"fedora-tp","_GID":"0","_MACHINE_ID>
{"_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","SYSLOG_PID":"22006","_CAP_EFFECTIVE":"3fffffffff","_SYSTEMD_UNIT":"sshd.service","_HOSTNAME":"fedora-tp",>
{"_COMM":"sshd","_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","SYSLOG_FACILITY":"10","_SYSTEMD_SLICE":"system.slice","_TRANSPORT":"syslog","SYSLOG_IDENTIFIER":"sshd","_>
{"SYSLOG_TIMESTAMP":"Dec  5 14:28:49 ","_EXE":"/usr/sbin/sshd","_PID":"22011","_SOURCE_REALTIME_TIMESTAMP":"1575556129634654","_SYSTEMD_SLICE":"system.slice","SYSLOG_PI>
{"SYSLOG_TIMESTAMP":"Dec  5 14:28:49 ","_EXE":"/usr/sbin/sshd","_TRANSPORT":"syslog","_SYSTEMD_CGROUP":"/system.slice/sshd.service","_SYSTEMD_UNIT":"sshd.service","_CAP>
{"_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","__REALTIME_TIMESTAMP":"1575556130062879","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=2348;b=58b4b45d76164318a82bc13>
{"SYSLOG_TIMESTAMP":"Dec  5 14:28:50 ","_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","MESSAGE":"pam_unix(sshd:session): session opened for user admin by (uid=0)","_SYST>
{"_SELINUX_CONTEXT":"system_u:system_r:sshd_t:s0-s0:c0.c1023","__MONOTONIC_TIMESTAMP":"16824106149","SYSLOG_TIMESTAMP":"Dec  5 14:28:50 ","MESSAGE":"pam_unix(sshd:sessi>
{"PRIORITY":"6","__MONOTONIC_TIMESTAMP":"16824424211","_PID":"22021","_TRANSPORT":"syslog","_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","_EXE":"/usr/sbin/sshd","_CMDLI>
{"_MACHINE_ID":"be1eb5b0480b41e294953b05585e2abd","_SOURCE_REALTIME_TIMESTAMP":"1575556130571307","MESSAGE":"pam_unix(sshd:session): session opened for user admin by (u>
{"_CMDLINE":"sshd: admin [priv]","_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=237a;b=58b4b45d76164318a82bc137f56d45ce;>
{"__MONOTONIC_TIMESTAMP":"16824911556","_HOSTNAME":"fedora-tp","_GID":"0","__REALTIME_TIMESTAMP":"1575556131019944","_SYSTEMD_SLICE":"system.slice","_CMDLINE":"sshd: ad>
{"_PID":"22051","_CAP_EFFECTIVE":"3fffffffff","_COMM":"sshd","_CMDLINE":"sshd: admin [priv]","SYSLOG_PID":"22051","_MACHINE_ID":"be1eb5b0480b41e294953b05585e2abd","_SYS>
{"_CMDLINE":"sshd: admin [priv]","_MACHINE_ID":"be1eb5b0480b41e294953b05585e2abd","_CAP_EFFECTIVE":"3fffffffff","_PID":"22051","SYSLOG_TIMESTAMP":"Dec  5 14:28:51 ","_E>
{"__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=23ab;b=58b4b45d76164318a82bc137f56d45ce;m=3eae2939e;t=598f5bf8e2982;x=dd890e74711d8a08","_SOURCE_REALTIME_TIMESTAMP":">
{"PRIORITY":"6","_SOURCE_REALTIME_TIMESTAMP":"1575556131766337","_TRANSPORT":"syslog","_MACHINE_ID":"be1eb5b0480b41e294953b05585e2abd","_PID":"22056","SYSLOG_PID":"2205>
{"_UID":"0","_BOOT_ID":"58b4b45d76164318a82bc137f56d45ce","__REALTIME_TIMESTAMP":"1575556131882375","_TRANSPORT":"syslog","_SOURCE_REALTIME_TIMESTAMP":"1575556131882343>
{"_HOSTNAME":"fedora-tp","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=23cc;b=58b4b45d76164318a82bc137f56d45ce;m=3eae98963;t=598f5bf951f46;x=b0a20ebca641d847","_CAP_>
{"MESSAGE":"pam_unix(sshd:session): session opened for user admin by (uid=0)","__MONOTONIC_TIMESTAMP":"16826115070","SYSLOG_FACILITY":"10","_PID":"22061","_SYSTEMD_UNIT>
{"__MONOTONIC_TIMESTAMP":"16826232131","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=23df;b=58b4b45d76164318a82bc137f56d45ce;m=3eaebed43;t=598f5bf978327;x=3ba307ae70>
{"_UID":"0","_EXE":"/usr/sbin/sshd","_SYSTEMD_SLICE":"system.slice","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=23ed;b=58b4b45d76164318a82bc137f56d45ce;m=3eb3f2545>
{"_PID":"22066","_COMM":"sshd","_SYSTEMD_INVOCATION_ID":"8bf7002f4b9143ea9815053c054cd3c3","__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=23f3;b=58b4b45d76164318a82bc>
{"__CURSOR":"s=2d9e6c6e0049468bb8e07d4b4f6bbcce;i=23fe;b=58b4b45d76164318a82bc137f56d45ce;m=3eb472286;t=598f5bff2b869;x=5b29b687f26557f5","_SYSTEMD_CGROUP":"/system.sli>
```

## III. Mécanismes manipulés par systemd

### 1. cgroups

#### affichez une structure arborescente des cgroups

``` bash
admin in ~ at fedora-tp
➜ systemd-cgls
Control group /:
-.slice
├─user.slice
│ └─user-1000.slice
│   ├─user@1000.service
│   │ └─init.scope
│   │   ├─911 /usr/lib/systemd/systemd --user
│   │   └─913 (sd-pam)
│   ├─session-4.scope
│   │ ├─13127 sshd: admin [priv]
│   │ ├─13130 sshd: admin@pts/0
│   │ ├─13131 -zsh
│   │ ├─22777 systemd-cgls
│   │ └─22778 less
│   └─session-1.scope
│     ├─1294 gpg-agent --daemon --use-standard-socket
│     └─1380 ssh-agent -s
├─init.scope
│ └─1 /usr/lib/systemd/systemd --switched-root --system --deserialize 28
└─system.slice
  ├─containerd.service
  │ └─647 /usr/bin/containerd
  ├─systemd-networkd.service
  │ └─12888 /usr/lib/systemd/systemd-networkd
  ├─systemd-udevd.service
  │ └─518 /usr/lib/systemd/systemd-udevd
  ├─dbus-broker.service
  │ ├─620 /usr/bin/dbus-broker-launch --scope system --audit
  │ └─621 dbus-broker --log 4 --controller 9 --machine-id be1eb5b0480b41e294953b05585e2abd --max-bytes 536870912 --max-fds 4096 --max-matches 131072 --audit
  ├─docker.service
  │ └─663 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
  ├─chronyd.service
  │ └─20830 /usr/sbin/chronyd
  ├─auditd.service
  │ └─593 /sbin/auditd
  ├─systemd-journald.service
  │ └─503 /usr/lib/systemd/systemd-journald
  ├─sshd.service
  │ └─674 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openssh.com,aes128-ctr,aes128-cbc -oMACs=hma>
  ├─firewalld.service
  │ └─613 /usr/bin/python3 /usr/sbin/firewalld --nofork --nopid
  ├─sssd.service
  │ ├─722 /usr/sbin/sssd -i --logger=files
  │ ├─732 /usr/libexec/sssd/sssd_be --domain implicit_files --uid 0 --gid 0 --logger=files
  │ └─737 /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=files
  ├─systemd-resolved.service
  │ └─20230 /usr/lib/systemd/systemd-resolved
  ├─system-getty.slice
  │ └─getty@tty1.service
  │   └─693 /sbin/agetty -o -p -- \u --noclear tty1 linux
  └─systemd-logind.service
    └─741 /usr/lib/systemd/systemd-logind
```

#### affichez un affichage comme `top` avec l'utilisation des ressources en temps réel, par cgroups

``` bash
Control Group                                            Tasks   %CPU   Memory  Input/s Output/s
/                                                          180    2.0     1.0G        -        -
user.slice                                                   8    1.2   281.0M        -        -
system.slice                                                54    0.1   640.7M        -        -
system.slice/containerd.service                             18    0.1    69.3M        -        -
system.slice/docker.service                                 19    0.0   127.2M        -        -
docker                                                       -      -    12.0K        -        -
init.scope                                                   1      -    53.9M        -        -
system.slice/auditd.service                                  2      -     6.9M        -        -
system.slice/boot.mount                                      -      -    12.0K        -        -
system.slice/chronyd.service                                 1      -     1.8M        -        -
system.slice/dbus-broker.service                             2      -     3.7M        -        -
system.slice/dev-hugepages.mount                             -      -   108.0K        -        -
system.slice/dev-…ra\x2d\x2dtp\x2d\x2dleo\x2dswap.swap       -      -   444.0K        -        -
system.slice/dev-mqueue.mount                                -      -    48.0K        -        -
system.slice/firewalld.service                               2      -    41.7M        -        -
system.slice/sshd.service                                    1      -     8.9M        -        -
system.slice/sssd.service                                    3      -    55.9M        -        -
system.slice/system-getty.slice                              1      -   760.0K        -        -
system.slice/system-getty.slice/getty@tty1.service           1      -   760.0K        -        -
```

#### ajoutez l'affichage des cgroups à `ps`

``` bash
admin in ~ at fedora-tp
➜ ps -e -o pid,cmd,cgroup
    PID CMD                         CGROUP
      1 /usr/lib/systemd/systemd -- 11:memory:/init.scope,9:blkio:/init.scope,6:cpu,cpuacct:/init.scope,4:pids:/init.scope,3:devices:/init.scope,1:name=systemd:/init.sco
      2 [kthreadd]                  -
      3 [rcu_gp]                    -
      4 [rcu_par_gp]                -
      6 [kworker/0:0H-kblockd]      -
      8 [mm_percpu_wq]              -
      9 [ksoftirqd/0]               -
     10 [rcu_sched]                 -
     11 [migration/0]               -
     13 [cpuhp/0]                   -
     14 [cpuhp/1]                   -
     15 [migration/1]               -
     16 [ksoftirqd/1]               -
     18 [kworker/1:0H-kblockd]      -
     19 [cpuhp/2]                   -
     20 [migration/2]               -
     21 [ksoftirqd/2]               -
     23 [kworker/2:0H-kblockd]      -
     24 [cpuhp/3]                   -
     25 [migration/3]               -
     26 [ksoftirqd/3]               -
     28 [kworker/3:0H-kblockd]      -
     29 [cpuhp/4]                   -
     30 [migration/4]               -
     31 [ksoftirqd/4]               -
     33 [kworker/4:0H-kblockd]      -
     34 [cpuhp/5]                   -
     35 [migration/5]               -
     36 [ksoftirqd/5]               -
     38 [kworker/5:0H-kblockd]      -
     39 [cpuhp/6]                   -
     40 [migration/6]               -
     41 [ksoftirqd/6]               -
     43 [kworker/6:0H-kblockd]      -
     44 [cpuhp/7]                   -
     45 [migration/7]               -
     46 [ksoftirqd/7]               -
     48 [kworker/7:0H-kblockd]      -
     49 [kdevtmpfs]                 -
     50 [netns]                     -
     51 [rcu_tasks_kthre]           -
     52 [kauditd]                   -
     55 [oom_reaper]                -
     56 [writeback]                 -
     57 [kcompactd0]                -
     58 [ksmd]                      -
     59 [khugepaged]                -
     82 [cryptd]                    -
    166 [kintegrityd]               -
    167 [kblockd]                   -
    168 [blkcg_punt_bio]            -
    169 [tpm_dev_wq]                -
    170 [ata_sff]                   -
    171 [md]                        -
    172 [edac-poller]               -
    173 [devfreq_wq]                -
    174 [watchdogd]                 -
    177 [kswapd0]                   -
    178 [kworker/u17:0]             -
    181 [kthrotld]                  -
    182 [acpi_thermal_pm]           -
    183 [scsi_eh_0]                 -
    184 [scsi_tmf_0]                -
    185 [scsi_eh_1]                 -
    186 [scsi_tmf_1]                -
    189 [dm_bufio_cache]            -
    190 [ipv6_addrconf]             -
    197 [kstrp]                     -
    329 [scsi_eh_2]                 -
    331 [scsi_tmf_2]                -
    344 [kworker/3:1H-kblockd]      -
    350 [kworker/6:1H-kblockd]      -
    351 [kworker/4:1H-kblockd]      -
    408 [kdmflush]                  -
    409 [kdmflush]                  -
    426 [xfsalloc]                  -
    427 [xfs_mru_cache]             -
    428 [xfs-buf/dm-1]              -
    429 [xfs-conv/dm-1]             -
    430 [xfs-cil/dm-1]              -
    431 [xfs-reclaim/dm-]           -
    432 [xfs-eofblocks/d]           -
    433 [xfs-log/dm-1]              -
    434 [xfsaild/dm-1]              -
    435 [kworker/5:1H-kblockd]      -
    436 [kworker/7:1H-xfs-log/dm-1] -
    502 [kworker/0:1H-kblockd]      -
    503 /usr/lib/systemd/systemd-jo 11:memory:/system.slice/systemd-journald.service,9:blkio:/system.slice/systemd-journald.service,6:cpu,cpuacct:/system.slice/systemd-j
    515 [kworker/2:1H-kblockd]      -
    518 /usr/lib/systemd/systemd-ud 11:memory:/system.slice/systemd-udevd.service,9:blkio:/system.slice/systemd-udevd.service,6:cpu,cpuacct:/system.slice/systemd-udevd.s
    568 [ttm_swap]                  -
    579 [xfs-buf/sda1]              -
    580 [xfs-conv/sda1]             -
    581 [xfs-cil/sda1]              -
    582 [xfs-reclaim/sda]           -
    583 [xfs-eofblocks/s]           -
    585 [xfs-log/sda1]              -
    586 [xfsaild/sda1]              -
    587 [kworker/1:1H-kblockd]      -
    593 /sbin/auditd                11:memory:/system.slice/auditd.service,9:blkio:/system.slice/auditd.service,6:cpu,cpuacct:/system.slice/auditd.service,4:pids:/system
    613 /usr/bin/python3 /usr/sbin/ 11:memory:/system.slice/firewalld.service,9:blkio:/system.slice/firewalld.service,6:cpu,cpuacct:/system.slice/firewalld.service,4:pid
    620 /usr/bin/dbus-broker-launch 11:memory:/system.slice/dbus-broker.service,9:blkio:/system.slice/dbus-broker.service,6:cpu,cpuacct:/system.slice/dbus-broker.service
    621 dbus-broker --log 4 --contr 11:memory:/system.slice/dbus-broker.service,9:blkio:/system.slice/dbus-broker.service,6:cpu,cpuacct:/system.slice/dbus-broker.service
    647 /usr/bin/containerd         11:memory:/system.slice/containerd.service,9:blkio:/system.slice/containerd.service,6:cpu,cpuacct:/system.slice/containerd.service,4:
    663 /usr/bin/dockerd -H fd:// - 11:memory:/system.slice/docker.service,9:blkio:/system.slice/docker.service,6:cpu,cpuacct:/system.slice/docker.service,4:pids:/system
    674 /usr/sbin/sshd -D -oCiphers 11:memory:/system.slice/sshd.service,9:blkio:/system.slice/sshd.service,6:cpu,cpuacct:/system.slice/sshd.service,4:pids:/system.slice
    693 /sbin/agetty -o -p -- \u -- 11:memory:/system.slice/system-getty.slice/getty@tty1.service,9:blkio:/system.slice/system-getty.slice,6:cpu,cpuacct:/system.slice/sy
    722 /usr/sbin/sssd -i --logger= 11:memory:/system.slice/sssd.service,9:blkio:/system.slice/sssd.service,6:cpu,cpuacct:/system.slice/sssd.service,4:pids:/system.slice
    732 /usr/libexec/sssd/sssd_be - 11:memory:/system.slice/sssd.service,9:blkio:/system.slice/sssd.service,6:cpu,cpuacct:/system.slice/sssd.service,4:pids:/system.slice
    737 /usr/libexec/sssd/sssd_nss  11:memory:/system.slice/sssd.service,9:blkio:/system.slice/sssd.service,6:cpu,cpuacct:/system.slice/sssd.service,4:pids:/system.slice
    741 /usr/lib/systemd/systemd-lo 11:memory:/system.slice/systemd-logind.service,9:blkio:/system.slice/systemd-logind.service,6:cpu,cpuacct:/system.slice/systemd-login
    911 /usr/lib/systemd/systemd -- 11:memory:/user.slice/user-1000.slice/user@1000.service,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.sl
    913 (sd-pam)                    11:memory:/user.slice/user-1000.slice/user@1000.service,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.sl
   1294 gpg-agent --daemon --use-st 11:memory:/user.slice/user-1000.slice/session-1.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slic
   1380 ssh-agent -s                11:memory:/user.slice/user-1000.slice/session-1.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slic
  12570 [kworker/4:2-events]        -
  12888 /usr/lib/systemd/systemd-ne 11:memory:/system.slice/systemd-networkd.service,9:blkio:/system.slice/systemd-networkd.service,6:cpu,cpuacct:/system.slice/systemd-n
  13127 sshd: admin [priv]          11:memory:/user.slice/user-1000.slice/session-4.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slic
  13130 sshd: admin@pts/0           11:memory:/user.slice/user-1000.slice/session-4.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slic
  13131 -zsh                        11:memory:/user.slice/user-1000.slice/session-4.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slic
  14277 [kworker/5:0-mm_percpu_wq]  -
  16409 [kworker/2:2-mm_percpu_wq]  -
  18912 [kworker/6:1-events]        -
  19315 [kworker/1:1-events]        -
  20136 [kworker/7:0-mm_percpu_wq]  -
  20230 /usr/lib/systemd/systemd-re 11:memory:/system.slice/systemd-resolved.service,9:blkio:/system.slice/systemd-resolved.service,6:cpu,cpuacct:/system.slice/systemd-r
  20828 [kworker/0:1-mm_percpu_wq]  -
  20830 /usr/sbin/chronyd           11:memory:/system.slice/chronyd.service,9:blkio:/system.slice/chronyd.service,6:cpu,cpuacct:/system.slice/chronyd.service,4:pids:/sys
  20977 [kworker/6:2-events]        -
  20978 [kworker/7:1-cgroup_destroy -
  21106 [kworker/5:1-cgroup_destroy -
  21964 [kworker/1:0-events]        -
  21975 [kworker/2:1-cgroup_destroy -
  22050 [kworker/0:0-events]        -
  22402 [kworker/u16:2-events_unbou -
  22802 [kworker/u16:1-events_unbou -
  22964 [kworker/3:0-xfs-cil/dm-1]  -
  23002 [kworker/3:1-xfs-cil/dm-1]  -
  23081 [kworker/4:1-ata_sff]       -
  23091 [kworker/6:0-events]        -
  23092 [kworker/6:3-mm_percpu_wq]  -
  23093 [kworker/3:2-mm_percpu_wq]  -
  23098 [kworker/4:0-ata_sff]       -
  23147 ps -e -o pid,cmd,cgroup     11:memory:/user.slice/user-1000.slice/session-4.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slic
```

#### Prenez le temps de vous balader un peu avec ces commandes, et d'explorer `/sys/fs/cgroup` pour voir les restrictions mises en place.

``` bash
admin in /sys/fs/cgroup  at fedora-tp
➜ ls
blkio  cpu  cpuacct  cpu,cpuacct  cpuset  devices  freezer  hugetlb  memory  net_cls  net_cls,net_prio  net_prio  perf_event  pids  systemd  unified

admin in /sys/fs/cgroup  at fedora-tp
➜ cat memory/memory.usage_in_bytes
921313280

admin in /sys/fs/cgroup  at fedora-tp
➜ cat memory/docker/memory.usage_in_byte
12288

admin in /sys/fs/cgroup  at fedora-tp
➜ cat systemd/user.slice/user-1000.slice/session-22.scope/cgroup.procs
43129
43132
43133
44200
```

#### 🌞 identifier le cgroup utilisé par votre session SSH

``` bash
➜ ps -e -o pid,cmd,cgroup | grep sshd
    674 /usr/sbin/sshd -D -oCiphers 11:memory:/system.slice/sshd.service,9:blkio:/system.slice/sshd.service,6:cpu,cpuacct:/system.slice/sshd.service,4:pids:/system.slice/sshd.service,3:devices:/system.slice/sshd.service,1:name=systemd:/system.slice/sshd.service,0::/system.slice/sshd.service
  43129 sshd: admin [priv]          11:memory:/user.slice/user-1000.slice/session-22.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slice/session-22.scope,3:devices:/user.slice,1:name=systemd:/user.slice/user-1000.slice/session-22.scope,0::/user.slice/user-1000.slice/session-22.scope
  43132 sshd: admin@pts/0           11:memory:/user.slice/user-1000.slice/session-22.scope,9:blkio:/user.slice,6:cpu,cpuacct:/user.slice,4:pids:/user.slice/user-1000.slice/session-22.scope,3:devices:/user.slice,1:name=systemd:/user.slice/user-1000.slice/session-22.scope,0::/user.slice/user-1000.slice/session-22.scope
```

##### identifier la RAM maximale à votre disposition (dans `/sys/fs/cgroup`)

``` bash
admin in /sys/fs/cgroup  at fedora-tp
➜ cat memory/user.slice/user-1000.slice/session-22.scope/memory.limit_in_bytes
9223372036854771712
```

#### 🌞 modifier la RAM dédiée à votre session utilisateur

- `systemctl set-property <SLICE_NAME> MemoryMax=512M`

``` bash
admin in /sys/fs/cgroup  at fedora-tp
➜ sudo systemctl set-property user-1000.slice MemoryMax=512M
```

#### 🌞 vérifier la création du fichier dans `/etc/systemd/system.control/`

- on peut supprimer ces fichiers pour annuler les changements

``` bash
admin in /sys/fs/cgroup  at fedora-tp
➜ cat /etc/systemd/system.control/user-1000.slice.d/50-MemoryMax.conf
# This is a drop-in unit file extension, created via "systemctl set-property"
# or an equivalent operation. Do not edit.
[Slice]
MemoryMax=536870912

admin in /sys/fs/cgroup  at fedora-tp
➜ sudo rm /etc/systemd/system.control/user-1000.slice.d/50-MemoryMax.conf
```

## La dissidence

> Alors que les questions sur les outils de systemd s'enchaînaient à vitesse grand V, le jeune Victor se sentait léger. Tout à coup, son corps s'éleva dans les airs. Les commandes apparurent devant ses yeux. Un sentiment de plénitude emplit son âme. Il ne faisait qu'un avec systemd.

> Après ce bref moment de transcendance post-tp-de-léo, il laissa la puissance du kernel linux le déposer calmement sur son siège, puis décida de se rebeller contre le système et de monter son serveur de musique à l'aide de son savoir fraîchement acquis.

### Pitch et architecture

Le jeune Victor possède un NAS tout neuf sous Archlinux avec ZFS hébergeant environ 400 Go de musique en FLAC (oui je sais, comparé à Clément, tout ça).

Il est également propriétaire d'un PC portable faisant office d'hyperviseur, tournant sous Proxmox et hébergeant quelques VMs.

Il souhaiterait exposer sur internet un serveur Funkwhale proposant la musique stockée sur le NAS et mettre à jour régulièrement la bibliothèque de Funkwhale pour ajouter les nouvelles musiques du NAS.

### Mise en place

Le réseau étant déjà configuré, il n'est pas nécessaire d'effectuer des modifications pour permettre aux nouveaux services de fonctionner. On rajoute juste des réservations DHCP, les règles firewall. Le NAS est également déjà fonctionnel mais ne partage rien.

Il faut mettre en place 3 VMs supplémentaires :

- une instance **Funkwhale**
- un **service d'ajout de musique**
- un **reverse proxy** proposant un endpoint https pour l'exposition de Funkwhale sur internet

Les VMs tourneront sous Archlinux parce que **cheh**.

Non en vrai, j'aime bien la légèreté du système et les rolling releases, surtout quand les services sont dans des conteneurs. Je sais qu'il existe toute une tripotée de systèmes spécial conteneurs, j'ai pas eu des super expériences avec, mais je suis ouvert. 😋

Allez, on monte tout ça.

![later](img/later.jpg)

En se basant sur l'architecture du réseau et en montant les quelques VMs de plus sur l'hyperviseur, on obtient l'infrastructure suivante :

![architecture](./img/archi-tp.jpg)

Le réseau est configuré à l'aide de **networkd** et **resolved** sur toutes les VMs et sur le NAS :

``` bash
victor_mignot in ~ at pere-blaise
➜ cat /etc/systemd/network/20-wired.network
[Match]
Name=ens18

[Network]
DHCP=ipv4
UseHostname=false
LinkLocalAddressing=no
IPv6AcceptRA=no
LLMNR=no

victor_mignot in ~ at pere-blaise
➜ cat /etc/systemd/resolved.conf
[Resolve]
#DNS=
FallbackDNS=
#Domains=
LLMNR=no
#MulticastDNS=yes
DNSSEC=yes
#DNSOverTLS=no
#Cache=yes
#DNSStubListener=yes
#ReadEtcHosts=yes
```

Voici la configuration du service **Funkwhale** par **docker-compose** :

``` bash
victor_mignot in ~ on 🐳 v19.03.5 at buzit
➜ cat /srv/funkwhale/docker-compose.yml
version: "3.7"

services:
  funkwhale:
    container_name: funkwhale
    restart: unless-stopped
    image: funkwhale/all-in-one:0.20.1
    environment:
      PUID: 976
      FUNKWHALE_HOSTNAME: funkwhale.victor-mignot.me
      NESTED_PROXY: 1
    volumes:
      - /srv/funkwhale/data:/data
      - /mnt/music:/music
    ports:
      - "80:80"

```

Voici la configuration du **reverse proxy Traefik** par **docker-compose** :

``` bash
victor_mignot in ~ on 🐳 v19.03.5 at yvain
➜ cat /srv/traefik/docker-compose.yml
version: '3.7'

services:
  traefik:
    image: 'traefik:2.0'
    restart: always
    ports:
      - 80:80
      - 443:443
    volumes:
      - ./config/traefik:/etc/traefik


victor_mignot in ~ on 🐳 v19.03.5 at yvain
➜ cat /srv/traefik/config/traefik/dynamic-conf.toml
[http.middlewares]
  [http.middlewares.redirect.redirectScheme]
    scheme = "https"


[http.routers]

  [http.routers.funkwhale0]
    rule = "Host(`funkwhale.victor-mignot.me`)"
    service = "funkwhale"
    entrypoints = ["web"]
    middlewares = ["redirect"]

  [http.routers.funkwhale1]
    rule = "Host(`funkwhale.victor-mignot.me`)"
    service = "funkwhale"
    entrypoints = ["web-secure"]
    [http.routers.funkwhale1.tls]
      certResolver = "CertificateResolver0"
      [[http.routers.funkwhale1.tls.domains]]
          main = "funkwhale.victor-mignot.me"


[http.services]

  [[http.services.funkwhale.loadBalancer.servers]]
    url = "http://192.168.2.69"

```

On allume tous ces services, on met en place la **règle NAT** vers le reverse proxy et magie ! 🌈🌞 Grâce à **traefik**, on a un endpoint https avec **certificat let's encrypt auto renouvelé**. Funkwhale est monté et exposé à l'extérieur, il ne manque plus que la musique.

Sur le NAS, on configure avec **NFS** et **ZFS** l'exposition d'un partage réseau pour la musique. Le partage doit être accessible en **lecture seule** par le serveur Funkwhale et en **lecture-écriture** par le service d'ajout de musique.

``` bash
victor_mignot in ~ at pere-blaise
➜ _ zpool status -v main
  pool: main
 state: ONLINE
  scan: scrub repaired 0B in 0 days 00:54:05 with 0 errors on Wed Dec 11 12:25:48 2019
config:

	NAME                                  STATE     READ WRITE CKSUM
	main                                  ONLINE       0     0     0
	  raidz1-0                            ONLINE       0     0     0
	    ata-ST4000VN008-2DR166_ZDH7YG3F   ONLINE       0     0     0
	    ata-ST4000VN008-2DR166_ZDH7ZEEZ   ONLINE       0     0     0
	    ata-TOSHIBA_HDWQ140_773RK0N3FPBE  ONLINE       0     0     0
	    ata-TOSHIBA_HDWQ140_773SK0NQFPBE  ONLINE       0     0     0

errors: No known data errors

victor_mignot in ~ at pere-blaise
➜ _ zpool list
NAME   SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP    HEALTH  ALTROOT
main  14.5T  4.31T  10.2T        -         -     1%    29%  1.00x    ONLINE  -

victor_mignot in ~ at pere-blaise
➜ _ zfs list
NAME            USED  AVAIL     REFER  MOUNTPOINT
main           3.13T  7.11T      186K  /mnt/main
main/data       362G  7.11T      362G  /mnt/main/data
main/movies    1.11T  7.11T     1.11T  /mnt/main/movies
main/music      378G  7.11T      378G  /mnt/main/music
main/pictures   110G  7.11T      110G  /mnt/main/pictures
main/tv-shows   883G  7.11T      883G  /mnt/main/tv-shows
main/videos     338G  7.11T      338G  /mnt/main/videos

victor_mignot in ~ at pere-blaise
➜ _ zfs set sharenfs="rw=@192.168.2.71,ro=@192.168.2.69" main/music

```

Sur le serveur Funkwhale et le service d'ajout, on configure le montage du partage et on active le montage au démarrage.

``` bash
victor_mignot in ~ at buzit
➜ cat /etc/systemd/system/mnt-music.mount
[Unit]
Description=Mount NAS music
Requires=network-online.target
After=network-online.target

[Mount]
What=192.168.4.10:/mnt/main/music
Where=/mnt/music
Type=nfs
Options=_netdev,auto,ro

[Install]
WantedBy=multi-user.target

victor_mignot in ~ at buzit
➜ sudo systemctl start mnt-music.mount

victor_mignot in ~ at buzit
➜ sudo systemctl enable mnt-music.mount
```

``` bash
victor_mignot in ~ at loth
➜ cat /etc/systemd/system/mnt-music.mount
[Unit]
Description=Mount NAS music
Requires=network-online.target
After=network-online.target

[Mount]
What=192.168.4.10:/mnt/main/music
Where=/mnt/music
Type=nfs
Options=_netdev,auto

[Install]
WantedBy=multi-user.target

victor_mignot in ~ at loth
➜ sudo systemctl start mnt-music.mount

victor_mignot in ~ at loth
➜ sudo systemctl enable mnt-music.mount
```

On redémarre le conteneur Funkwhale **et voilà !** La musique est disponible dans les conteneurs Funkwhale et d'ajout, si **on ajoute une musique dans le montage** sur le serveur d'ajout, **elle apparaît immédiatement dans le conteneur Funkwhale.**

``` bash
victor_mignot in ~ at loth
➜ touch /mnt/music/super-musique.flac
```

``` bash
victor_mignot in ~ at buzit
➜ sudo docker exec -it funkwhale sh
/ # cd /music
/music # ls -la super-musique.flac
-r--r--r--    1 1000     funkwhal         0 Dec 12 00:38 super-musique.flac
```

Il faut ensuite ajouter le contenu du dossier à l'application Funkwhale.

``` bash
victor_mignot in ~ at buzit
➜ sudo docker exec -it funkwhale python /app/api/manage.py import_files "*ID_LIBRARY*" "/music/**/*.flac" --recursive --no-input --in-place
```

Ça y est ! Funkwhale a importé les musiques et il est désormais possible de l'écouter partout dans le monde ! 🌍 🎧 📻 🎷 🎸 🎹 🎺 🎻

Il est maintenant intéressant d'ajouter périodiquement le nouveau contenu du partage à l'instance Funkwhale. Il faut donc créer un service et un timer.

``` bash
victor_mignot in ~ at buzit
➜ cat /etc/systemd/system/update-funkwhale-music.service
[Unit]
Description=update of funkwhale library from nas mount
Requires=network-online.target mnt-music.mount docker.service

[Service]
Type=oneshot
ExecStart=docker exec funkwhale python /app/api/manage.py import_files "*ID_LIBRARY*" "/music/**/*.flac" --recursive --no-input --in-place

victor_mignot in ~ at buzit
➜ cat /etc/systemd/system/update-funkwhale-music.timer  
[Unit]
Description=hourly update of funkwhale library

[Timer]
OnCalendar=hourly
Persistent=true

[Install]
WantedBy=timers.target

victor_mignot in ~ at buzit
➜ sudo systemctl start update-funkwhale-music.timer

victor_mignot in ~ at buzit
➜ sudo systemctl enable update-funkwhale-music.timer
Created symlink /etc/systemd/system/timers.target.wants/update-funkwhale-music.timer → /etc/systemd/system/update-funkwhale-music.timer.

victor_mignot in ~ at buzit took 2s
➜ sudo systemctl status update-funkwhale-music.service
● update-funkwhale-music.service - update of funkwhale library from nas mount
     Loaded: loaded (/etc/systemd/system/update-funkwhale-music.service; static; vendor preset: disabled)
     Active: inactive (dead) since Thu 2019-12-12 01:00:55 CET; 49min ago
TriggeredBy: ● update-funkwhale-music.timer
   Main PID: 17863 (code=exited, status=0/SUCCESS)

déc. 12 01:00:55 buzit docker[17863]: Successfully imported 196 tracks
déc. 12 01:00:55 buzit docker[17863]: For details, please refer to import reference 'cli-2019-12-12T00:00:46.978277+00:00' or URL http://funkwhale.victor-mignot.me/content/>
déc. 12 01:00:55 buzit systemd[1]: update-funkwhale-music.service: Succeeded.
déc. 12 01:00:55 buzit systemd[1]: Started update of funkwhale library from nas mount.

victor_mignot in ~ at buzit
➜ sudo systemctl status update-funkwhale-music.timer  
● update-funkwhale-music.timer - hourly update of funkwhale library
     Loaded: loaded (/etc/systemd/system/update-funkwhale-music.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Wed 2019-12-11 16:48:51 CET; 9h ago
    Trigger: Thu 2019-12-12 02:00:00 CET; 9min left
   Triggers: ● update-funkwhale-music.service

déc. 11 16:48:51 buzit systemd[1]: Started hourly update of funkwhale library.
```

**MISSION ACCOMPLIE**

![success](img/success.gif)
